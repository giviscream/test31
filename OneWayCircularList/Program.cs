﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneWayCircularList
{
    class Program
    {
        //So this is a metanit.com realization of circular list.
        static void Main(string[] args)
        {
            CircularLinkedList<string> circularList = new CircularLinkedList<string>();

            circularList.Add("Tom");
            circularList.Add("Bob");
            circularList.Add("Alice");
            circularList.Add("Jack");
            foreach (var item in circularList)
            {
                Console.WriteLine(item);
            }
        }
    }
}
